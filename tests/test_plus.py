import pytest

from third import plus

def test_plus():
    assert plus(2, 2) == 4

def test_plus_zeroes():
    assert plus(0 , 0) == 0

def test_plus_with_zero():
    assert plus(0, 5) == 5


if __name__ == '__main__':
    pytest.main()