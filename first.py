import requests
from bs4 import BeautifulSoup
from time import sleep

# url = 'https://scrapingclub.com/exercise/list_basic/?page=1'

'''
method 'find' gets value from a teg
method 'get' gets value from a attribute
'''
handlers = {"user-agent":  # site will get an answer from the program with this data
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.4 Safari/605.1.15"}

for count in range(1,8):
    sleep(3)  # program will make a request every 3 second
    url = f'https://scrapingclub.com/exercise/list_basic/?page={count}'

    response = requests.get(url, headers=handlers)  # all data from html page

    soup = BeautifulSoup(response.text, "lxml")  # first parameter is values, second parameter is the parser

    # data = soup.find('div', class_='col-lg-4 col-md-6 mb-4') # parser will wind "div" with some class + but only first value
    #
    # name = data.find('h4', class_='card-title').text.replace('\n', '')  # attribute 'text' takes only text in line, and 'replace' deletes an excess \n-space
    # price = data.find('h5').text
    # url_img = 'https://scrapingclub.com' + data.find('img', class_='card-img-top img-fluid').get('src')

    data = soup.find_all('div', class_='col-lg-4 col-md-6 mb-4') # method 'find_all' gets all data from algorithm

    for i in data:
        name = i.find('h4', class_='card-title').text.replace('\n', '')
        price = i.find('h5').text
        url_img = 'https://scrapingclub.com' + i.find('img', class_='card-img-top img-fluid').get('src')
        print(name + '\n' + price + '\n' + url_img + '\n\n')

