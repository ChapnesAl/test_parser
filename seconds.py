import requests
from bs4 import BeautifulSoup
from time import sleep

# url = 'https://scrapingclub.com/exercise/list_basic/?page=1'

'''
method 'find' gets value from a teg
method 'get' gets value from a attribute
'''
handlers = {"user-agent":
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.4 Safari/605.1.15"}

for count in range(1,8):
    sleep(3)
    url = f'https://scrapingclub.com/exercise/list_basic/?page={count}'

    response = requests.get(url, headers=handlers)

    soup = BeautifulSoup(response.text, "lxml")


    data = soup.find_all('div', class_='col-lg-4 col-md-6 mb-4')
    # for i in data:


